const app = require('express')();
let serveStatic = require('serve-static');
let bodyParser = require('body-parser');
const cookieParser = require("cookie-parser");

global.vApp = {};
vApp.config = global.config = require("./conf/development.json");

const firebase = require('firebase/app');
const firebaseAuth = require('firebase/auth');
const admin = require('firebase-admin');
const firebaseConfig = config.firebaseConfig;
const auth = firebaseAuth.getAuth(firebaseApp);
const expiresIn = 60 * 60 * 24 * 5 * 1000;

const firebaseApp = firebase.initializeApp(firebaseConfig);
admin.initializeApp(firebaseConfig);


// set the view engine to ejs
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(serveStatic(__dirname + '/public'));

console.log('listening on port 8080');

app.get('/', function(req, res) {
    res.render('index');
});

app.get('/map', async function(req, res) {
    try {
        const sessionCookie = req.cookies.session || "";

        if(!sessionCookie) res.redirect('/');

        await admin.auth().verifyIdToken(sessionCookie);

        res.render('map');
    } catch(ex) {
        console.log(ex);
        res.redirect('/');
    }
    
});

app.post('/signin', async function(req, res) {
    try {
        let username = req.body.username;
        let password = req.body.password;

        let userCredential = await firebaseAuth.signInWithEmailAndPassword(auth, username, password);
        let user = userCredential.user;

        let idToken = await user.getIdToken();
        res.cookie("session", idToken, { maxAge: expiresIn, httpOnly: true });
        console.log(idToken);
        res.end(JSON.stringify({ status: "success" }));
    } catch(ex) {
        console.log(ex);
        res.end(JSON.stringify({ status: "failed" }));
    }
    
});

app.get("/signout", (req, res) => {
    res.clearCookie("session");
    res.redirect("/");
  });

app.listen(config.port, function () {
    console.log(`Test UI listening on port ${config.port}!`);
});