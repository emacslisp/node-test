
class TestController {
    constructor(){
        return this;
    }

    async test(req, res, next) {
        res.json({'success': true});
    }
}

module.exports = TestController;