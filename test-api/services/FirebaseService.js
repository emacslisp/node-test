const firebase = require('firebase/app');
const firebaseAuth = require('firebase/auth');
const firestore = require('firebase/firestore');
const firebaseConfig = config.firebaseConfig;
const app = firebase.initializeApp(firebaseConfig);
const auth = firebaseAuth.getAuth(app);
const db = firestore.getFirestore(app);
const usersCollection = firestore.collection(db, 'users');

class FirebaseService {
    constructor(){
        return this;
    }

    /**
     * check username/password is authenticated
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async isAuthentication(req, res, next) {
        try {
            if (req.headers.authorization) {
                const bufferBase64 = req.headers.authorization;
                let decodedKey = Buffer.from(bufferBase64.split(' ')[1], 'base64').toString('utf-8');
                let username = decodedKey.split(':')[0];
                let password = decodedKey.split(':')[1];

                let user = await vApp.FirebaseService.verifyCredential(username, password);
                if(!user) {
                    res.status(401).json({ error: 'Username or Password is invalid' });
                } else {
                    req.user = user;
                }

            } else {
                return res.status(401).json({ error: 'Token expired or invalid' });
            }
        }
        catch(err) {
            return res.status(401).json({ error: 'Token expired or invalid' });
        }

        return next();   
    }

    /**
     * check user's role
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */
    async isAdmin(req, res, next) {
        if(req.user)  {
            if(req.user.role === 'admin') {
                return next();
            } else {
                return res.status(401).json({ error: 'User is read-only' });
            }
        } else {
            return res.status(401).json({ error: 'Username or Password is invalid' });
        }

        return next();
    }

    /**
     * verify user's credential
     * @param {string} username 
     * @param {string} password 
     * @returns {object} user
     */
    async verifyCredential(username, password) {
        let userCredential = await firebaseAuth.signInWithEmailAndPassword(auth, username, password);
        let user = userCredential.user;

        const q = firestore.query(usersCollection, firestore.where('email', '==', username));
        const result = await firestore.getDocs(q);
        const list = result.docs.map(doc => doc.data());
        if(list.length === 1) {
            user.role = list[0].role;
        }

        return user;
    }
}

module.exports = FirebaseService;
