express = require('express');
app = express();
router = express.Router();

const cors = require('cors');
bodyParser = require('body-parser');
jsonParser = bodyParser.json();

global.vApp = {};
vApp.debug = console;
global.config = require('./environments/development.json');

app.use(bodyParser.json({ type: 'application/json' }));
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }))
app.use(bodyParser.text({ type: 'text/html' }));

require('./route/router.js');

app.get('/', function (req, res) {
    res.send('no content!')
  });
  
app.listen(config.port, function () {
    console.log(`Test API listening on port ${config.port}!`);
});