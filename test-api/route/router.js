let TestController = require('../controllers/TestController.js');
vApp.TestController = new TestController();

let FirebaseService = require('../services/FirebaseService.js');
vApp.FirebaseService = new FirebaseService();

app.get('/api/admin_only',jsonParser, vApp.FirebaseService.isAuthentication, vApp.FirebaseService.isAdmin, vApp.TestController.test);